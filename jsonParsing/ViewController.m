//
//  ViewController.m
//  jsonParsing
//
//  Created by Click Labs130 on 10/29/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{

}
- (void)viewDidLoad {
    [super viewDidLoad];
/*
 NSDictionary *json  = [NSDictionary dictionaryWithObjectsAndKeys:@"Authentication Successful", @"auth_response",@"abc123", @"auth_token",  @"Niteesh Sharma", @"Name", @"ios Developer", @"designation", nil];
    
    if (json [@"auth_response"]!=nil) {
        NSString *responce= json[@"auth_response"];
      NSLog(@"%@",responce);
    }
    if (json [@"auth_token"]!=nil) {
        NSString *token = json[@"auth_token"];
        NSLog(@"%@", token);
    }
    if (json[@"Name"]!=nil) {
        NSString *name = json[@"Name"];
        NSLog(@"%@", name);
    }
    if(json[@"designation"]!=nil)
    {
        NSString *work = json[@"designation"];
        NSLog(@"%@", work);
    }
    // Do any additional setup after loading the view, typically from a nib.
 */
    NSString *path = [[NSBundle mainBundle] pathForResource:@"jsonFile" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSDictionary *coord=json[@"coord"];
    NSString *latitude = coord[@"lat"];
    NSLog(@"%@",  latitude);
    
    NSString *longitude= coord[@"lon"];
    NSLog(@"%@", longitude);
    
    NSDictionary *weather=json[@"main"];
    NSNumber *pressure = weather[@"pressure"];
    NSLog(@"%@",pressure);
    
    
     NSDictionary *sys=json[@"sys"];
    NSString *msg = sys[@"message"];
    NSLog(@"%@",  msg);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
